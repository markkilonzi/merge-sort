
class Merger {

    private int[] mylist;
    private int[] temp;

    int[] sort(int[] array) {

        this.mylist = array;
        int array_length = array.length;
        this.temp = new int[array_length];
        mergesort(0, array_length - 1);

        return mylist;
    }

    private void mergesort(int low, int high) {
        {
            if (low < high) {

                int middle = low + (high - low) / 2;

                mergesort(low, middle);
                mergesort(middle + 1, high);
                merge(low, middle, high);
            }
        }

    }
    private void merge(int low, int middle, int high) {

        for (int i = low; i <= high; i++) {
            temp[i] = mylist[i];
        }

        int i = low;
        int j = middle + 1;
        int k = low;

        while (i <= middle && j <= high) {
            if (temp[i] <= temp[j]) {
                mylist[k] = temp[i];
                i++;
            } else {
                mylist[k] = temp[j];
                j++;
            }
            k++;
        }
        while (i <= middle) {
            mylist[k] = temp[i];
            k++;
            i++;
        }

    }

}
