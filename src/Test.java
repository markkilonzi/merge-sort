import java.util.Arrays;
import java.util.Random;


public class Test extends Merger{

    private static int [] myList = new int[11];

    private int[] generateRandomList() {
        int pick,i;

        Random rand = new Random();
        for(i=0; i<11; i++){
            pick = rand.nextInt(100);
            Test.myList[i] = pick;
        }

        return myList;
    }

    private void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }

    public static void  main(String[] args){

        int [] answer;
        int [] initial;
        Test test = new Test();
        System.out.println("Initial Randomized Array");
        initial = test.generateRandomList();
        test.printArray(initial);
        answer = test.sort(initial);
        System.out.println("Final Sorted Array");
        test.printArray(answer);

    }
}
